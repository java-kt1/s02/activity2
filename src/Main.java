import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        int[] firstFivePrimeNumber = new int[5];

        firstFivePrimeNumber[0] = 2;
        firstFivePrimeNumber[1] = 3;
        firstFivePrimeNumber[2] = 5;
        firstFivePrimeNumber[3] = 7;
        firstFivePrimeNumber[4] = 11;

        System.out.println("The first prime number is: " + firstFivePrimeNumber[0]);


        ArrayList<String> friends = new ArrayList<String>();

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>();

        inventory.put("toothbrush", 20);
        inventory.put("toothpaste", 15);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);


    }
}